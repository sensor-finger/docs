# MuJoCo

Do wizualizacji został wykorzystany silnik [MuJoCo](https://mujoco.org/) o otwartym kodzie źródłowym, który pozwala na szybką symulację projektów z dziedziny robotyki oraz biomechaniki. Wykorzystując wcześniej zaimplementowane funkcjonalności dotyczące [komunikacji](/oprogramowanie/komunikacja) i [odczytu kątów](/oprogramowanie/odczyt-katow) dokonano integracji oprogramowania z środowiskiem symluacyjnym.

## Migracja modelu 3D

Pierwszym niezbędnym krokiem do realizacji wizualizacji była migracja stworzonej wcześniej [konstrukcji nośnej](/mechanika/konstrukcja-nosna) z programu **SolidWorks** do  **Autodesk Fusion 360**. Taki zabieg umożliwił zastosowanie [skrypu](https://github.com/syuntoku14/fusion2urdf), który dokonał konwersji modelu do formatu **URDF** *(Unified Robotics Description Format)*. Plik URDF zawiera pozycję poszczególych elementów modelu oraz zdefiniowne zależości między nimi takie jak typy połączenia, ograniczenia zakresów ruchu i pozycje początkowe.

![fusion_finger](/images/wizualizacja/fusion_finger.png)

## Kompilacja i symulacja modelu

Za pośrednictwem programu udostępnianego przez MuJoCo dokonano kompilacji otrzymanego wcześniej modelu w formacie URDF. Plikiem wynikowym kompilacji jest plik o rozszerzeniu `.xml`, który jest już bezpośrednio wczytywany do symulacji.

```bash
~/.mujoco/mujoco210/bin/compile  model.urdf model.xml
```

Posiadając plik `.xml` uruchomiono symulację. MuJoCo udostępnia interfejs umożliwający poruszanie wcześniej zdefiniowanymi przegubami oraz szereg innych zaawansowanych funkcjonalności.

```bash
~/.mujoco/mujoco210/bin/simulate model.xml
```

![sim](/images/wizualizacja/sim.png)

## Integracja oprogramowania

Kolejnym etapem realizacji wizualizacji była integracaja oraz rozwój dotychczasowo powstałego oprogramowania. Tym sposobem uzyskano program, który odbiera kąty z fizycznego modelu palca, odpowiednio je przetwarza i odwzorowuje ruch rzeczywistego palca w środowisku symulacyjnym.

### Pliki konfiguracyjne

W plikach konfiguracyjnyych zostały zawarte nazwy połączeń pomiędzy poszczególnymi przegubami, które są tożsame z nazwami zdefiniowanymi na etapie tworzenia modelu. Znajdują się tutaj również informacje na temat kierunków kątów. Palec sensoryczny umożliwia pomiar czterech kątów, pozostałe z nich są wyliczane w programie na podstawie dobranych współczynników.

![joints_config](/images/wizualizacja/joints_config.png)

Dodatkowo zostały zdefiniowane zmienne, które przechowują ścieżki plików niezbędnych do działania programu.

![definitions](/images/wizualizacja/definitions.png)

### Funkcje

W odzielnym pliku zostały zaimplementowane funkcje pozwalające na wczytywanie pliku w formacie `.xml`, ładowanie wcześniej opisanej konfiguracji oraz na odpowiednie pobieranie nazw poszczególnych przegubów.

![functions](/images/wizualizacja/functions.png)

Najważniejszą funkcją z tego modułu jest funkcja obliczająca wartości dla odpowiednich przegubów. Jako agrument przyjmuje ona dane odbierane z enkoderów, następnie na ich podstawie dokonuje obliczeń i zwraca wartości kątów, które kolejno są ustawiane w odpowiadających im przegubach. 

![calculate](/images/wizualizacja/calculate_joints.png)

Wymienione wyżej funkcje są wywoływane w głównym module programu, gdzie następuje renderowanie modelu w środowisku symulacyjnym i aktualizacja pozycji palca.

![main](/images/wizualizacja/main.png)

## Działająca wizualizacja

Efektem końcowym działającej wizualizacji jest adekwatna zmiana pozycji modelu palca w symulacji do ruchów wykonywanch fizycznym palcem użytkownika. Prezentacja działania programu znajduje się na ponniższym filmie.

[![subs_video](/images/wizualizacja/vis_video.png)](https://www.youtube.com/watch?v=JxqALmFJtic&list=PLefnzpZAt87gSKysLupQRZX_EB9Flmhv3&index=2&ab_channel=Kania)

