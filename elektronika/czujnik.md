# Czujniki

Do odczytu kątów zostały użyte enkodery magnetyczne. Rozdzielczość ich odczytu to 12 bitów, więc ich zakres od 0 do 4095. Dane z enkoderów do mikrokontrolera są przekazywane za pomocą interfejsu `I2C`. Wszystkie enkodery mają stały, jednakowy adres. Magnes użyty do odczytu ma bieguny ułożone jak na rysunku niżej.

 ![magnes](/images/elektronika/bieguny_magnes.png)

Dzięki takiemu rozkładowi biegunów jest możliwe ułożenie czujnika oraz magnesu obok siebie w taki sposób, że oś przechodząca przez środek magnesu pokrywa się z osią przechodzącą przez środek czujnika. 
Do obsługi wszystkich przegubów w palcu potrzeba czterech czujników. 
