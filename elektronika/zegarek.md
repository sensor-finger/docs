# Zegarek
Na zegarku zostały umieszczone wszystkie potrzebne elementy do obsługi czujników i zbierania z nich surowych danych. Głównym elementem zegarka jest mikrokontroler of firmy ST. Na nim znajduje się oprogramowanie zbierające dane i wysyłające je dalej przez magistrale CAN. Następnym elementem jest multiplekser `I2C`. Wszystkie czujniki mają ten sam adres, przez co nie można podłączyć czujników bezpośrednio do mikrokontrolera. Sam mikrokontroler nie posiada zaimplementowanego modułu do obsługi magistrali CAN, przez co potrzebny jest osobny moduł do jego obsługi. Na zdjęciu niżej widać wszystkie elektroniczne elementy. 

![elektronika](/images/elektronika/zegarek_elektronika.jpg)

Z drugiej strony płytki znajdują się złącze śrubowe, piny potrzebne do programowania mikrokontrolera oraz złącza na przewody. W złączu śrubowym dwa piny są przeznaczone do zasilania, a dwa kolejne do wysyłania informacji po magistrali CAN. Dolna linia złącz odpowiada za zasilanie czujników. Aktualnie jest podpięty jeden przewód zasilający do pierwszego czujnika, a reszta czujników jest podpięta do niego. Złącza znajdujące się wyżej są przeznaczone dla linii sygnałowych czujników. Maksymalnie może być podpięte 16 czujników (co odpowiada zbieraniu danych z 4 palców). Aktualnie są podłączone 4 czujniki, co pozwala nam odczytywać dane z jednego palca. Na rysunku niżej zaprezentowano drugą stronę zegarka.

![zlacza](/images/elektronika/zegarek_zlacza.jpg)
