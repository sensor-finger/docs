# Oprogramowanie
Oprogramowanie stworzono w języku C na mikrokontrolerze STM32 z serii F. Do komunkiacji wykorzystano magistralę CAN, za pomocą której przesyłane są odczytane dane z poszczególnych enkoderów. Poniżej zaprezentowano pseudokod, który przybliży ideę działania całego programu.

![PSEUDOKOD](/images/oprogramowanie/pseudocode.png)


