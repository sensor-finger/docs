.. Palec sensoryczny documentation master file, created by
   sphinx-quickstart on Sun May 22 20:26:23 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Witaj na stronie dokumentacji Palca Sensorycznego
=================================================

Mechanika
---------

Konstrukcja nośna rękawiczki została zaprojektowana i zamodelowana w programie SolidWorks. Po weryfikacji poprawności projektu z członkami zespołu wydrukowano prototyp konstrukcji na drukarce 3D. Następnie zweryfikowano poprawność konstrucji, zidentyfikowano pojawiające się błędy oraz naniesiono poprawki do modelu. Finalnie wydrukowano i złożono poprawioną wersję.

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Mechanika

   mechanika/konstrukcja-nosna.md

Elektronika
-----------

Podstawowymi elementami w elektronice są czujniki, zegarek odczytujący dane 
i oprogramowanie mikrokontrolera. Do komunikacji wykorzystano magistrale I2C 
oraz CAN. Na poszczególnych podstronach można zobaczyć opis kazdego z modułów
wraz z zastosowanymi rozwiązaniami. 

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Elektronika

   elektronika/czujnik.md
   elektronika/zegarek.md
   elektronika/oprogramowanie.md

Oprogramowanie
--------------

W skład oprogramowania wchodzą dwa główne moduły - odczytywanie kątów za pomocą
enkoderów oraz komunikacja RPi wraz ze światem zewnętrznym. Pierwszy z modułów
jest kluczowy ze względu na samą ideę projektu. Komunikacja została zrobiona na
wzór komunikacji występującej w ROSie. Raspberry Pi, które odczytuje kąty
publikuje je na jednym adresie IP, a wiele urządzeń może te informacje
subskrybować.

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Oprogramowanie

   oprogramowanie/odczyt-katow.md
   oprogramowanie/komunikacja.md

Wizualizacja
--------------

Wizualizacja pozwala zaobserwować efekt końcowy projektu. Stworzony wcześniej model
palca został zaimportowany do silnika symulacyjnego. Ruch fizycznym palcem, a tym samym
zmiana wartości kątów poszczególnych przegubów, jest odwzorowywany przez model
w środowisku symulacyjnym.

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Wizualizacja

   wizualizacja/mujoco.md
