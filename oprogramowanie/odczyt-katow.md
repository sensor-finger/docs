# Odczyt kątów

Odczytywanie kątów z autorskiej konstrukcji mechanicznej zaimplementowane zostało dzięki komunikacji ,,zegarka" wraz z komputerem jednopłytkowym Raspberry Pi 4 poprzez magistralę CAN. Zaprojektowana elektronika wysyła surowe odczyty brzpośrednio z enkoderów magnetycznych. Z tego powodu pierwszym zadaniem w tym dziale projektu było odczytanie surowych danych oraz przetworzenie ich do postaci bardziej przystępnej. Po wstępnym ich przetworzeniu należało wysłać te dane dalej. Ten aspekt projektu można przeczytać [tutaj](/oprogramowanie/komunikacja).

## Komunikacja poprzez magistralę CAN
Zastosowany komputer nie posiada wbudowanej magistrali CAN, z tego względu aby umożliwić taką komunikację zastosowano nakładkę RS485 CAN HAT do komputerów RaspberryPi.

![can_hat](/images/oprogramowanie/can_hat.jpg)

Zastosowanie takiej nakładki wymagało zainstalowania i skonfigurowania interfejsu `can0`. W tym celu postępowano zgodnie z instrukcjami zamieszczonymi na tej [stronie](https://www.pragmaticlinux.com/2021/10/can-communication-on-the-raspberry-pi-with-socketcan/).

## Odczytywanie i przeliczanie kątów
Zadanie to zostało zaimplementowane przy pomocy osób z firmy zewnętrznej. Nie możemy udostępnić w pełni kodu źródłowego tego etapu, dlatego poniżej można zobaczyć pseudokod tego rozwiązania.

![read_process_angles](/images/oprogramowanie/read_process_angles.png)

Program ten realizuje odczyt danych w sposób asynchroniczny z interfejsu `can0`. Następnie przetwarza odebrane dane - parsuje surowe odczyty kątów oraz przelicza te wartości na stopnie. W momencie uruchomienia programu, zakłada on, że ręka jest w położeniu zerowym, a wszystkie ruchy wykonane po starcie są różnicą między stanem obecnym a położeniem zerowym.

## Automatyczne odczytywanie kątów przy starcie
W celu ułatwienia interakcji z komputerem Raspberry Pi zaimplementowano serwis linuxowy oparty o `systemd`, który uruchamia się automatycznie wraz ze startem systemu. Konfiguruje on interfejs systemowy oraz wykonuje program do oczytywania kątów oraz ich przeliczania.

![service_linux](/images/oprogramowanie/service_linux.png)

![service_journal](/images/oprogramowanie/servis_journal.png)
