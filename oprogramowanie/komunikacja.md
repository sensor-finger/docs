# Komunikacja
Celem tego zadania było umożliwienie komunikacji między komputerem odczytującym dane z enkoderów a światem zewnętrznym. Podczas implementacji tego zadania wzorowano się na sposobie komunikacji w narzędziu ROS. Raspberry Pi, które oczutyje kąty jest swogo rodzaju publisherem i rozgłaśnia informacje o odczytanych danych na jednym z socketów. Każdy komputer może się do tego socketu podłączyć i subskrybować dane, które go interesują. Do wykonania tego zadania wykorzystano framework **ZeroMQ** do jezyka **Python**.

## Wysyłanie danych
Ponownie program do wysyłania danych jest bardzo mocno zintegorwany z programem do odczytu kątów. Z tego powodu poniżej zamieszczony jest pseudokod przedstawiający ideę tego rozwiązania.

![read_sent_data](/images/oprogramowanie/read_sent_data.png)

Skrypt ten identycznie odczytuje dane jak w [tym module](/oprogramowanie/odczyt-katow). Dodatkową funkcjonalnością jest jednak wysyłanie tych danych na zewnątrz. Umożliwia to w łatwy i przejrzysty sposób pozyskanie takich danych przez wiele odbiorców i opracowywanie ich na różne sposoby.

## Odbieranie danych przez klientów
W celu odbierania danych został napisany kolejny program, tym razem na wszystke komputery, aby każdy mógł obierać wysyłane odczyty kątów. Program ten ma na celu podłączenie się do wcześniej skonfigurowanego socketu na Raspberry Pi, a następnie subskrybować dane, które zawierają temat _,,Angles"_. Na zdjęciu poniżej przedstawiony jest przykładowy program realizujący ww założenia.

![subscriber](/images/oprogramowanie/subscriber.png)

Przykład działania tego programu zamieszczony jest na filmie poniżej.

[![subs_video](/images/oprogramowanie/subs_video.png)](https://www.youtube.com/watch?v=XetTite1ZL4&ab_channel=Kania)
