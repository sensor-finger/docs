# Konstrukcja nośna
Konstrukcja mechaniczna palca zamodelowana została w programie SolidWorks. Wyposażona została ona w 4 czujniki obrotu, co pozwala śledzić wszystkie stopnie swobody pojedynczego palca. Rękawice zaprojektowo jako nakładkę na dłoń, mocowaną za pomocą elastycznej gumy. Nasada dłoni oraz każdy paliczek mają osobną nakładkę, stanowiącą uchwyt na enkoder. Połączone zostały przegubami, w których końcach umieszczono magnesy. Każdemu ruchowi paliczka odpowiada obrót odpowiedniego przegubu. Dzięki temu można śledzić wychyły palca w każdym kierunku. Poniżej przedstawiono zrzuty ekranu zamodelowanej konstrukcji.

![model1](/images/mechanika/model1.png)


![model2](/images/mechanika/model2.png)

Ze względu na duży stopnień skomplikowania kontrukcji zdecydowano się na wykonanie jej na drukarce 3D w technologii SLA. Pozwoliło to na zachowanie wysokiej dokładności detali przy stosunkowo szybkim czasie ich wykonania. Problematyczna okazała się kruchość elementów, dlatego należało wykazać się szczególną ostrożnością w trakcie montażu czujników i magnesów oraz późniejszym testowaniu konstrukcji. Na poniższych zdjęciach widoczna jest złozona konstrukcja.

![wydruk1](/images/mechanika/wydruk1.png)


![wydruk2](/images/mechanika/wydruk2.png)
